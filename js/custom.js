// Commenting to check that this file loaded

$(window).scroll(
    {
        previousTop: 0
    },
    function () {
    var currentTop = $(window).scrollTop();
    if (currentTop < this.previousTop) {
        //show
        $("nav#navbar-main").css("top", "0px");
    } else {
        //hide
        $("nav#navbar-main").css("top", "-82px");
    }
    this.previousTop = currentTop;
});

    function getScheduleList(districtKey){
        // TODO: Make this more robust. This will not make AJAX request if I ever put anything inside the fold
        var id = '#fold_' + districtKey.toString();
        if ($(id).is(':empty')){
            $(id).load('/nca/district/' + districtKey.toString());
        };
    };

function slugify(text){
  return text.toString().toLowerCase()
    .replace(/(\w)\'/g, '$1')           // Special case for apostrophes
    .replace(/[^a-z0-9_\-]+/g, '-')     // Replace all non-word chars with -
    .replace(/\-\-+/g, '-')             // Replace multiple - with single -
    .replace(/^-+/, '')                 // Trim - from start of text
    .replace(/-+$/, '');                // Trim - from end of text
};
